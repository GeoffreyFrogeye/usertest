package main

import (
	"github.com/pion/rtcp"
	"github.com/pion/webrtc/v2"
	"github.com/pion/webrtc/v2/pkg/media"
	"github.com/pion/webrtc/v2/pkg/media/ivfwriter"
	"github.com/pion/webrtc/v2/pkg/media/opuswriter"
	"encoding/base64"
	"encoding/json"
	email "github.com/cameronnewman/go-emailvalidation"
	"github.com/ant0ine/go-json-rest/rest"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net/http"
	"github.com/goodes/go-json-rest-middleware-jwt"
	"time"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"os/exec"
	"io/ioutil"
	"os"
)

func main() {
	fmt.Println("Started on localhost:3033")
	i := Impl{}
	i.InitDB()
	i.InitSchema()
	defer i.DB.Close()

	jwt_middleware := &jwt.JWTMiddleware{
		Key:        []byte(os.Getenv("SECRET_KEY")),
		Realm:      "jwt auth",
		Timeout:    time.Hour,
		MaxRefresh: time.Hour * 24,
		Authenticator: func(userId string, password string) bool {
			user := User{}
			if i.DB.Where("username = ?", userId).First(&user).Error != nil {
				return false
			}

			res := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password))
			fmt.Println(res)
			return res == nil
		}}

	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	api.Use(&rest.IfMiddleware{
		Condition: func(request *rest.Request) bool {
			if request.Method == "GET" && request.Header.Get("Authorization") == "" {
				return false
			}

			if request.URL.Path == "/login" {
				return false
			}

			if request.Method == "POST" && request.URL.Path == "/users" {
				return false
			}

			return true
		},
		IfTrue: jwt_middleware,
	})
	router, err := rest.MakeRouter(
		rest.Post("/login", jwt_middleware.LoginHandler),
		rest.Get("/refresh", jwt_middleware.RefreshHandler),

		rest.Get("/users/:id", i.GetUser),
		rest.Post("/users", i.PostUser),
		rest.Put("/users/:id", i.PutUser),
		rest.Delete("/users/:id", i.DeleteUser),

		rest.Get("/users/:id/projects", i.ListProjectsForUser),
		rest.Get("/projects", i.ListProjects),
		rest.Get("/projects/:id", i.GetProject),
		rest.Post("/projects", i.PostProject),
		rest.Put("/projects/:id", i.PutProject),
		rest.Delete("/projects/:id", i.DeleteProject),

		rest.Get("/projects/:id/tasks", i.ListTasksForProject),
		rest.Get("/tasks/:id", i.GetTask),
		rest.Post("/tasks", i.PostTask),
		rest.Put("/tasks/:id", i.PutTask),
		rest.Delete("/tasks/:id", i.DeleteTask),

		rest.Get("/tasks/:id/tests", i.ListTestsForTask),
		rest.Get("/tests", i.ListTests),
		rest.Get("/tests/:id", i.GetTest),
		rest.Post("/tests", i.PostTest),
		rest.Put("/tests/:id", i.PutTest),
		rest.Delete("/tests/:id", i.DeleteTest),
		rest.Post("/tests/:id/stream", i.WebRtc),

		rest.Get("/comments", i.ListComments),
		rest.Get("/comments/:id", i.GetComment),
		rest.Post("/comments", i.PostComment),
		rest.Put("/comments/:id", i.PutComment),
		rest.Delete("/comments/:id", i.DeleteComment),
	)
	if err != nil {
		log.Fatal(err)
	}
	api.SetApp(router)
	http.Handle("/api/v1/", http.StripPrefix("/api/v1", api.MakeHandler()))
	http.Handle("/media/", http.StripPrefix("/media", http.FileServer(http.Dir("media"))))
	http.HandleFunc("/upload", uploadFile)
	http.Handle("/", http.FileServer(FrontEnd{}))

	log.Fatal(http.ListenAndServe(":3033", nil))
}

type FrontEnd struct{}

func (f FrontEnd) Open(path string) (http.File, error) {
	file, err := os.Open("dist" + path)
	if err == nil {
		return file, nil
	}

	return os.Open("dist/index.html")
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	// TODO: require authentication for this route
    r.ParseMultipartForm(2 << 20) // max 2MB
    file, _, err := r.FormFile("file")
    if err != nil {
    	w.Write([]byte("{ \"error\": \"From error\" }"))
		return
    }
    defer file.Close()

    // Create a temporary file within our temp-images directory that follows
    // a particular naming pattern
    dest, err := ioutil.TempFile("media", "*")
    if err != nil {
        w.Write([]byte("{ \"error\": \"IO error\" }"))
        return
    }
    defer dest.Close()

    // read all of the contents of our uploaded file into a
    // byte array
    fileBytes, err := ioutil.ReadAll(file)
    if err != nil {
        w.Write([]byte("{ \"error\": \"IO Error\" }"))
    }
    // write this byte array to our temporary file
    dest.Write(fileBytes)

    w.Write([]byte(fmt.Sprint("{ \"url\": \"/", dest.Name(), "\" }")))
}

type User struct {
	gorm.Model
	Username string `gorm:"unique;not null" json:"userName"`
	DisplayName string `json:"displayName"`
	Email string `gorm:"unique;not null" json:"email"`
	PasswordHash string `gorm:"not null" json:"-"`
	Password string `gorm:"-" json:"password"`
	Bio string `json:"bio"`
	AvatarUrl string `json:"avatarUrl"`
	Confirmed bool `json:"-"`
	OwnerOf []*Project `gorm:"many2many:project_owners;" json:"-"`
	ContribTo []*Project `gorm:"many2many:project_contribs;" json:"-"`
	BannedFrom []*Project `gorm:"many2many:project_banned;" json:"-"`
	Tests []Test `json:"-"`
	Comments []Comment `json:"-"`
}

type Project struct {
	gorm.Model
	Name string `gorm:"unique;not null" json:"name"`
	Description string `json:"description"`
	IconUrl string `json:"iconUrl"`
	Owners []*User `gorm:"many2many:project_owners;PRELOAD:true" json:"owners"`
	Contribs []*User `gorm:"many2many:project_contribs;PRELOAD:true" json:"contributors"`
	Banned []*User `gorm:"many2many:project_banned;" json:"-"`
	Tasks []Task `gorm:"PRELOAD:true" json:"tasks"`
}

type Task struct {
	gorm.Model
	Title string `gorm:"unique;not null" json:"title"`
	Description string `json:"description"`
	TimeEstimation uint `json:"timeEstimation"` // in minutes
	ProjectID uint `json:"projectID"`
	Tests []Test `json:"tests"`
}

type Test struct {
	gorm.Model
	TaskID uint `json:"task"`
	UserID uint `json:"user"`
	Comments []Comment `gorm:"PRELOAD:true" json:"comments"`
	Status string `json:"status"` // "in progress" | "to review" | "reviewed" | "handled" | "closed"
	CanChangeStatus bool `gorm:"-" json:"canChangeStatus"`
}

type Comment struct {
	gorm.Model
	TestID uint `json:"test"`
	UserID uint `json:"user"`
	Content string `json:"content"`
	Edited bool `json:"edited"`
}

type Impl struct {
	DB *gorm.DB
}

func (i *Impl) InitDB() {
	var err error
	i.DB, err = gorm.Open("postgres", os.Getenv("DB_URL"))
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
	i.DB.LogMode(true)
}

func (i *Impl) InitSchema() {
	i.DB.AutoMigrate(&User{})
	i.DB.AutoMigrate(&Project{})
	i.DB.AutoMigrate(&Task{})
	i.DB.AutoMigrate(&Test{})
	i.DB.AutoMigrate(&Comment{})
}

// (I have no idea how modules work in go, so I will just put everything in the same file)

////////////////////////////////////////
// USERS
////////////////////////////////////////

func (i *Impl) GetUser(w rest.ResponseWriter, r *rest.Request) {
	id, err := strconv.ParseUint(r.PathParam("id"), 10, 32)

	if err != nil {
		user := User{}
		if i.DB.Where("username = ?", r.PathParam("id")).First(&user).Error != nil {
			rest.NotFound(w, r)
			return
		}

		w.WriteJson(&user)
	} else {
		user := User{}
		if i.DB.First(&user, id).Error != nil {
			rest.NotFound(w, r)
			return
		}
		w.WriteJson(&user)
	}
}

func (i *Impl) PostUser(w rest.ResponseWriter, r *rest.Request) {
	newUser := User{}
	if err := r.DecodeJsonPayload(&newUser); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(newUser.Password) < 8 {
		rest.Error(w, "Password is too short", http.StatusInternalServerError)
		return
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), 10)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := email.New().ValidateFormat(newUser.Email); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	new := User{
		Username: newUser.Username,
		DisplayName: newUser.Username,
		Email: newUser.Email,
		PasswordHash: string(pass),
	}
	if err := i.DB.Save(&new).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteJson(&new)
}

func (i *Impl) PutUser(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	user := User{}
	if i.DB.First(&user, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	if user.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't edit someone else's profile", http.StatusUnauthorized)
		return
	}

	updated := User{}
	if err := r.DecodeJsonPayload(&updated); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(updated.Password) < 8 && len(updated.Password) > 0 {
		rest.Error(w, "Password is too short", http.StatusInternalServerError)
		return
	}

	if err := email.New().ValidateFormat(updated.Email); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user.Username = updated.Username
	user.DisplayName = updated.DisplayName
	user.Email = updated.Email
	user.AvatarUrl = updated.AvatarUrl
	user.Bio = updated.Bio

	if len(updated.Password) >= 8 {
		pass, err := bcrypt.GenerateFromPassword([]byte(updated.Password), 10)
		if err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user.PasswordHash = string(pass)
	}

	if err := i.DB.Save(&user).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteJson(&user)
}

func (i *Impl) DeleteUser(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	user := User{}
	if i.DB.First(&user, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	if user.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't delete someone else's profile", http.StatusUnauthorized)
		return
	}

	if err := i.DB.Delete(&user).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

////////////////
// PROJECTS
////////////////


func (i *Impl) ListProjects(w rest.ResponseWriter, r *rest.Request) {
	projects := []Project{}
	i.DB.Preload("Tasks").Find(&projects)
	w.WriteJson(&projects)
}

type ProjectsForUser struct {
	OwnerOf []Project `json:"ownerOf"`
	ContributedTo []Project `json:"contributedTo"`
}

func (i *Impl) ListProjectsForUser(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	user := User{}
	if i.DB.First(&user, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := []Project{}
	i.DB.Model(&user).Preload("Tasks").Related(&owner, "OwnerOf")
	contrib := []Project{}
	i.DB.Model(&user).Preload("Tasks").Related(&contrib, "ContribTo")

	w.WriteJson(&ProjectsForUser {
		OwnerOf: owner,
		ContributedTo: contrib,
	})
}

func (i *Impl) GetProject(w rest.ResponseWriter, r *rest.Request) {
	id, err := strconv.ParseUint(r.PathParam("id"), 10, 32)

	if err != nil {
		project := Project{}
		if i.DB.Preload("Tasks").Preload("Contribs").Preload("Owners").Find(Project{ Name: r.PathParam("id") }).First(&project).Error != nil {
			rest.NotFound(w, r)
			return
		}
		w.WriteJson(&project)
	} else {
		project := Project{}
		if i.DB.Preload("Tasks").Preload("Contribs").Preload("Owners").First(&project, id).Error != nil {
			rest.NotFound(w, r)
			return
		}
		w.WriteJson(&project)
	}
}

func (i *Impl) PostProject(w rest.ResponseWriter, r *rest.Request) {
	project := Project{}
	if err := r.DecodeJsonPayload(&project); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := i.DB.Save(&project).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user := User{}
	if i.DB.Where("username = ?", r.Env["REMOTE_USER"]).First(&user).Error != nil {
		rest.NotFound(w, r)
		return
	}
	i.DB.Model(&user).Association("OwnerOf").Append(&project)

	w.WriteJson(&project)
}

func (i *Impl) PutProject(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	project := Project{}
	if i.DB.First(&project, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owners := []User{}
	_ = i.DB.Model(&project).Related(&owners, "Owners")
	for _, owner := range owners {
		if owner.Username == r.Env["REMOTE_USER"] {
			updated := Project{}
			if err := r.DecodeJsonPayload(&updated); err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			project.Name = updated.Name
			project.Description = updated.Description
			project.IconUrl = updated.IconUrl

			if err := i.DB.Save(&project).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteJson(&project)
			return
		}
	}

	rest.Error(w, "You can't edit a project that is not yours.", http.StatusUnauthorized)
}

func (i *Impl) DeleteProject(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	project := Project{}
	if i.DB.First(&project, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owners := []User{}
	_ = i.DB.Model(&project).Related(&owners, "Owners")
	for _, owner := range owners {
		if owner.Username == r.Env["REMOTE_USER"] {
			if err := i.DB.Delete(&project).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			return
		}
	}

	rest.Error(w, "You can't delete a project that is not yours.", http.StatusUnauthorized)
}

/////////////////
// Tasks
/////////////////

func (i *Impl) ListTasksForProject(w rest.ResponseWriter, r *rest.Request) {
	tasks := []Task{}
	projectID, _ := strconv.ParseUint(r.PathParam("id"), 10, 32)
	i.DB.Where(Task{ ProjectID: uint(projectID) }).Find(&tasks)
	w.WriteJson(&tasks)
}

func (i *Impl) GetTask(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	task := Task{}
	if i.DB.First(&task, id).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&task)
}

func (i *Impl) PostTask(w rest.ResponseWriter, r *rest.Request) {
	task := Task{}
	if err := r.DecodeJsonPayload(&task); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	project := Project{}
	if i.DB.First(&project, task.ProjectID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owners := []User{}
	_ = i.DB.Model(&project).Related(&owners, "Owners")
	for _, owner := range owners {
		if owner.Username == r.Env["REMOTE_USER"] {
			if err := i.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteJson(&task)
			return
		}
	}

	rest.Error(w, "You can't create a task for a project that is not yours.", http.StatusUnauthorized)
}

func (i *Impl) PutTask(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	task := Task{}
	if i.DB.First(&task, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	updated := Task{}
	if err := r.DecodeJsonPayload(&updated); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	task.Title = updated.Title
	task.Description = updated.Description
	task.TimeEstimation = updated.TimeEstimation

	project := Project{}
	if i.DB.First(&project, task.ProjectID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owners := []User{}
	_ = i.DB.Model(&project).Related(&owners, "Owners")
	for _, owner := range owners {
		if owner.Username == r.Env["REMOTE_USER"] {
			if err := i.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteJson(&task)
			return
		}
	}

	rest.Error(w, "You can't edit a task for a project that is not yours.", http.StatusUnauthorized)
}

func (i *Impl) DeleteTask(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	task := Task{}
	if i.DB.First(&task, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	project := Project{}
	if i.DB.First(&project, task.ProjectID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owners := []User{}
	_ = i.DB.Model(&project).Related(&owners, "Owners")
	for _, owner := range owners {
		if owner.Username == r.Env["REMOTE_USER"] {
			if err := i.DB.Delete(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			return
		}
	}

	rest.Error(w, "You can't edit a task for a project that is not yours.", http.StatusUnauthorized)
}

//////////////
// Tesys
/////////////

func (i *Impl) ListTestsForTask(w rest.ResponseWriter, r *rest.Request) {
	taskID, _ := strconv.ParseUint(r.PathParam("id"), 10, 32)
	tests := []Test{}
	i.DB.Where(&Test { TaskID: uint(taskID) }).Find(&tests)
	w.WriteJson(&tests)
}

func (i *Impl) ListTests(w rest.ResponseWriter, r *rest.Request) {
	tests := []Test{}
	i.DB.Find(&tests)
	w.WriteJson(&tests)
}

func (i *Impl) GetTest(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	test := Test{}
	if i.DB.Preload("Comments").First(&test, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := User{}
	if i.DB.First(&owner, test.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}
	canEdit := owner.Username == r.Env["REMOTE_USER"]
	if !canEdit {
		task := Task{}
		if i.DB.First(&task, test.TaskID).Error != nil {
			rest.NotFound(w, r)
			return
		}
		project := Project{}
		if i.DB.Preload("Owners").Find(&project, task.ProjectID).Error != nil {
			rest.NotFound(w, r)
			return
		}
		for _, po := range project.Owners {
			if po.Username == r.Env["REMOTE_USER"] {
				canEdit = true
				break
			}
		}
	}
	test.CanChangeStatus = canEdit

	w.WriteJson(&test)
}

var webRtcSessions = map[uint]*webrtc.PeerConnection{}

func (i *Impl) PostTest(w rest.ResponseWriter, r *rest.Request) {
	test := Test{}
	if err := r.DecodeJsonPayload(&test); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := i.DB.Save(&test).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	task := Task{}
	if err := i.DB.Find(&task, test.TaskID).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	project := Project{}
	if err := i.DB.Find(&project, task.ProjectID).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user := User{}
	if i.DB.Where("username = ?", r.Env["REMOTE_USER"]).First(&user).Error != nil {
		rest.NotFound(w, r)
		return
	}
	i.DB.Model(&user).Association("ContribTo").Append(&project)

	w.WriteJson(&test)

	// Create the WebRTC session //

	// Create a MediaEngine object to configure the supported codec
	m := webrtc.MediaEngine{}

	// Setup the codecs you want to use.
	// We'll use a VP8 codec but you can also define your own
	m.RegisterCodec(webrtc.NewRTPOpusCodec(webrtc.DefaultPayloadTypeOpus, 48000))
	m.RegisterCodec(webrtc.NewRTPVP8Codec(webrtc.DefaultPayloadTypeVP8, 90000))

	// Create the API object with the MediaEngine
	api := webrtc.NewAPI(webrtc.WithMediaEngine(m))

	// Everything below is the Pion WebRTC API! Thanks for using it ❤️.

	// Prepare the configuration
	config := webrtc.Configuration{
		ICEServers: []webrtc.ICEServer{
			{
				URLs: []string{"stun:stun.l.google.com:19302"},
			},
		},
	}

	// Create a new RTCPeerConnection
	peerConnection, err := api.NewPeerConnection(config)
	if err != nil {
		panic(err)
	}

	// Allow us to receive 1 audio track, and 1 video track
	if _, err = peerConnection.AddTransceiver(webrtc.RTPCodecTypeAudio); err != nil {
		panic(err)
	} else if _, err = peerConnection.AddTransceiver(webrtc.RTPCodecTypeVideo); err != nil {
		panic(err)
	}

	opusFile, err := opuswriter.New(fmt.Sprint("media/", test.ID, ".opus"), 48000, 2)
	if err != nil {
		panic(err)
	}
	ivfFile, err := ivfwriter.New(fmt.Sprint("media/", test.ID, ".ivf"))
	if err != nil {
		panic(err)
	}

	// Set a handler for when a new remote track starts, this handler saves buffers to disk as
	// an ivf file, since we could have multiple video tracks we provide a counter.
	// In your application this is where you would handle/process video
	peerConnection.OnTrack(func(track *webrtc.Track, receiver *webrtc.RTPReceiver) {
		// Send a PLI on an interval so that the publisher is pushing a keyframe every rtcpPLIInterval
		go func() {
			ticker := time.NewTicker(time.Second * 3)
			for range ticker.C {
				errSend := peerConnection.WriteRTCP([]rtcp.Packet{&rtcp.PictureLossIndication{MediaSSRC: track.SSRC()}})
				if errSend != nil {
					fmt.Println(errSend)
				}
			}
		}()

		codec := track.Codec()
		if codec.Name == webrtc.Opus {
			fmt.Println("Got Opus track, saving to disk (48 kHz, 2 channels)")
			saveToDisk(opusFile, track)
		} else if codec.Name == webrtc.VP8 {
			fmt.Println("Got VP8 track, saving to disk")
			saveToDisk(ivfFile, track)
		}
	})

	// Set the handler for ICE connection state
	// This will notify you when the peer has connected/disconnected
	peerConnection.OnICEConnectionStateChange(func(connectionState webrtc.ICEConnectionState) {
		fmt.Printf("Connection State has changed %s \n", connectionState.String())

		if connectionState == webrtc.ICEConnectionStateConnected {
			fmt.Println("New client connection")
		} else if connectionState == webrtc.ICEConnectionStateFailed ||
			connectionState == webrtc.ICEConnectionStateDisconnected {
			fmt.Println("Connection ended")

			closeErr := opusFile.Close()
			if closeErr != nil {
				panic(closeErr)
			}

			closeErr = ivfFile.Close()
			if closeErr != nil {
				panic(closeErr)

			}

			fmt.Println("Done writing media files")
			go func() {
				ffmpeg := exec.Command("ffmpeg", "-i", fmt.Sprint("media/", test.ID, ".ivf"), "-i", fmt.Sprint("media/", test.ID, ".opus"), "-vcodec", "copy", "-acodec", "copy", "-strict", "-2", fmt.Sprint("media/", test.ID, ".webm"), "-y")
				ffmpeg.Start()
				ffmpeg.Wait()
				fmt.Println("Done converting media files")
			}()

			delete(webRtcSessions, test.ID)
		}
	})

	webRtcSessions[test.ID] = peerConnection
}

func (i *Impl) PutTest(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	test := Test{}
	if i.DB.First(&test, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := User{}
	if i.DB.First(&owner, test.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	canEdit := owner.Username == r.Env["REMOTE_USER"]
	if !canEdit {
		task := Task{}
		if i.DB.First(&task, test.TaskID).Error != nil {
			rest.NotFound(w, r)
			return
		}
		project := Project{}
		if i.DB.Preload("Owners").First(&project, task.ProjectID).Error != nil {
			rest.NotFound(w, r)
			return
		}
		for _, po := range project.Owners {
			if po.Username == r.Env["REMOTE_USER"] {
				canEdit = true
				break
			}
		}
	}

	if !canEdit {
		rest.Error(w, "You can't edit a test that you didn't created.", http.StatusUnauthorized)
		return
	}

	updated := Test{}
	if err := r.DecodeJsonPayload(&updated); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	test.Status = updated.Status

	if err := i.DB.Save(&test).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteJson(&test)
}

func (i *Impl) DeleteTest(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	test := Test{}
	if i.DB.First(&test, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := User{}
	if i.DB.First(&owner, test.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	if owner.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't edit a test that you didn't created.", http.StatusUnauthorized)
		return
	}

	if err := i.DB.Delete(&test).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func saveToDisk(i media.Writer, track *webrtc.Track) {
	defer func() {
		if err := i.Close(); err != nil {
			panic(err)
		}
	}()

	for {
		rtpPacket, err := track.ReadRTP()
		if err != nil {
			panic(err)
		}
		if err := i.WriteRTP(rtpPacket); err != nil {
			panic(err)
		}
	}
}

type WebRtcData struct {
	Description string `json:"description"`
}

func (i *Impl) WebRtc(w rest.ResponseWriter, r *rest.Request) {
	id, _ := strconv.ParseUint(r.PathParam("id"), 10, 32)
	peerConnection := webRtcSessions[uint(id)]

	desc := WebRtcData{}
	if err := r.DecodeJsonPayload(&desc); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Wait for the offer to be pasted
	offer := webrtc.SessionDescription{}
	Decode(desc.Description, &offer)

	// Set the remote SessionDescription
	err := peerConnection.SetRemoteDescription(offer)
	if err != nil {
		panic(err)
	}

	// Create answer
	answer, err := peerConnection.CreateAnswer(nil)
	if err != nil {
		panic(err)
	}

	// Sets the LocalDescription, and starts our UDP listeners
	err = peerConnection.SetLocalDescription(answer)
	if err != nil {
		panic(err)
	}

	w.WriteJson(&WebRtcData{
		Description: Encode(answer),
	})
}

// Encode encodes the input in base64
// It can optionally zip the input before encoding
func Encode(obj interface{}) string {
	b, err := json.Marshal(obj)
	if err != nil {
		panic(err)
	}

	return base64.StdEncoding.EncodeToString(b)
}

// Decode decodes the input from base64
// It can optionally unzip the input after decoding
func Decode(in string, obj interface{}) {
	b, err := base64.StdEncoding.DecodeString(in)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(b, obj)
	if err != nil {
		panic(err)
	}
}

//////////////////
// Comments
/////////////////

func (i *Impl) ListComments(w rest.ResponseWriter, r *rest.Request) {
	comments := []Comment{}
	i.DB.Find(&comments)
	w.WriteJson(&comments)
}

func (i *Impl) GetComment(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	comment := Comment{}
	if i.DB.First(&comment, id).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&comment)
}

func (i *Impl) PostComment(w rest.ResponseWriter, r *rest.Request) {
	comment := Comment{}
	if err := r.DecodeJsonPayload(&comment); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	owner := User{}
	if i.DB.First(&owner, comment.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}
	if owner.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't edit a post a comment on behalf of someone else.", http.StatusUnauthorized)
		return
	}

	if err := i.DB.Save(&comment).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteJson(&comment)
}

func (i *Impl) PutComment(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	comment := Comment{}
	if i.DB.First(&comment, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := User{}
	if i.DB.First(&owner, comment.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	if owner.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't edit a comment that you didn't created.", http.StatusUnauthorized)
		return
	}

	updated := Comment{}
	if err := r.DecodeJsonPayload(&updated); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	comment.Content = updated.Content
	comment.Edited = true

	if err := i.DB.Save(&comment).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteJson(&comment)
}

func (i *Impl) DeleteComment(w rest.ResponseWriter, r *rest.Request) {
	id := r.PathParam("id")
	comment := Comment{}
	if i.DB.First(&comment, id).Error != nil {
		rest.NotFound(w, r)
		return
	}

	owner := User{}
	if i.DB.First(&owner, comment.UserID).Error != nil {
		rest.NotFound(w, r)
		return
	}

	if owner.Username != r.Env["REMOTE_USER"] {
		rest.Error(w, "You can't edit a comment that you didn't created.", http.StatusUnauthorized)
		return
	}

	if err := i.DB.Delete(&comment).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
