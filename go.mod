module framagit.org/AnaGelez/usertest

go 1.12

require (
	github.com/StephanDollberg/go-json-rest-middleware-jwt v0.0.0-20160723210644-be2a0500d9b3 // indirect
	github.com/cameronnewman/go-emailvalidation v1.1.127-0a57a1c // indirect
	github.com/gomarkdown/markdown v0.0.0-20190802011031-b3dd8ce84a34 // indirect
	github.com/goodes/go-json-rest-middleware-jwt v0.0.0-20190507165832-7e3b6f9f1cf8 // indirect
	github.com/jinzhu/gorm v1.9.10 // indirect
	github.com/pion/webrtc/v2 v2.1.2 // indirect
)
